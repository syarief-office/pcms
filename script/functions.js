jQuery(document).ready(function() {
  // Stickty Navigation
  /*var sticky_navigation_offset_top = jQuery('.header').offset().top;
  var sticky_navigation = function(){
  var scroll_top = jQuery(window).scrollTop(); // our current vertical position from the top
    if (scroll_top > sticky_navigation_offset_top) { 
      jQuery('.header').addClass( "fixed" );
      jQuery('body.checkout-page .header').removeClass('fixed');
    } else {
      jQuery('.header').removeClass( "fixed" );
    }   
  };
  sticky_navigation();
  jQuery(window).scroll(function() {
   sticky_navigation();
  });*/

 /*==========================================================================
    UPDATE JS
  ========================================================================== */
  //checkbox And Radio Butto 
  jQuery('input[type=checkbox], input[type=radio]').customRadioCheck();
  
  // Smooth Scroll
   jQuery('#arrDwon').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

          var target = jQuery(this.hash);
          target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            jQuery('html,body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
    });

  // Main menu active state
  if( jQuery('body.tax-news_category').length > 0 ) {
    jQuery('.subpage li.page-item-4269').addClass('current_page_item');
  }

  if( jQuery('body.tax-mvn_shop_composers').length > 0 ) { // Composers detail page
    jQuery('.subpage li.page-item-3723').addClass('current_page_item');
  }

  if( jQuery('body.tax-gallery_category').length > 0 || jQuery('body.tax-video_category').length > 0 || jQuery('body.tax-mvn_shop_composers').length > 0 ) {
    jQuery('#nav li.learn').addClass('current-menu-item');
  }

  if( jQuery('body.single-scholar').length > 0 ) {
    jQuery('#nav li.learn').addClass('current-menu-item');
    jQuery('.subpage li.page-item-3719').addClass('current_page_ancestor');
    jQuery('.subpage ul.children li.page-item-3732').addClass('current_page_item');
  }
    if( jQuery('body.single-artist').length > 0 ) {
    jQuery('#nav li.artists').addClass('current-page-ancestor');
  }
  if( jQuery('body.single-news').length > 0 ) {
    jQuery('.container li.page-item-4269').addClass('current-page-ancestor');
  }


  if( jQuery('body.tax-video_category').length > 0 ) {
    jQuery('.subpage li.page-item-3721').addClass('current_page_item');
  }

  // Split menu on megamanu into 2 columns
  jQuery('.megamenu li').wrapAll('<div class="clearfix">');
  jQuery('.megamenu div > li').addClass('column');
  jQuery('.megamenu li:last-child').css({'float':'right', 'border-left':'1px solid #ccc', 'padding-left':'5%'});

  // Tabs
  
  jQuery('.single-artist .tabs li').removeAttr('class');
  jQuery('.single-artist .tabs li:first').addClass('active');
  jQuery('.single-artist .tab-content').hide();
  jQuery('.single-artist .tab-content:first').show();
  jQuery('.single-artist .tabs li a').click(function(){
    jQuery('.single-artist .tabs li').removeAttr('class');
    jQuery(this).parent().addClass('active'); 
    var currentTab = jQuery(this).attr('href'); 
    jQuery('.single-artist .tab-content').hide();
    jQuery(currentTab).fadeIn();
    return false;
  });
  //tab concert
  jQuery('.single-mvn_product .tabs li').removeAttr('class');
  jQuery('.single-mvn_product .tabs li:first').addClass('active');
  jQuery('.single-mvn_product .tab-content').hide();
  jQuery('.single-mvn_product .tab-content:first').show();
  jQuery('.single-mvn_product .tabs li a').click(function(){
    jQuery('.single-mvn_product .tabs li').removeAttr('class');
    jQuery(this).parent().addClass('active'); 
    var currentTab = jQuery(this).attr('href'); 
    jQuery('.single-mvn_product .tab-content').hide();
    jQuery(currentTab).fadeIn();
    return false;
  });

  // navbar
  var subscribeForm = jQuery('#subscribe-form'),
      searchForm = jQuery('#search-form');
  jQuery('.subscribe-wrap a').click(function(){
    jQuery(this).toggleClass('active');
    jQuery(this).children().toggleClass('active');
    subscribeForm.fadeToggle();
    searchForm.fadeOut();
    jQuery('.search-wrap a').removeClass('active');
    jQuery('.search-wrap a').children().removeClass('active');
  })
  jQuery('.search-wrap a').click(function(){
    jQuery(this).toggleClass('active');
    jQuery(this).children().toggleClass('active');
    searchForm.fadeToggle();
    subscribeForm.fadeOut();
    jQuery('.subscribe-wrap a').removeClass('active');
    jQuery('.subscribe-wrap a').children().removeClass('active');
  });
  // jQuery('.subscribe-wrap a').click(function(){
  //   jQuery(this).toggleClass('active');
  //   jQuery(this).children().toggleClass('active');
  //   jQuery('#subscribe-form').fadeToggle();
  //   return false;
  // });
  // jQuery('.search-wrap a').click(function(){
  //   jQuery(this).toggleClass('active');
  //   jQuery(this).children().toggleClass('active');
  //   jQuery('#search-form').fadeToggle();
  //   return false;
  // });

  // Gravity Forms
  jQuery('#field_3_1 input').attr('placeholder', jQuery('#field_3_1 label.gfield_label').contents(':not(span)').text());
  jQuery('#field_3_2 input').attr('placeholder', 'Email Address');
  jQuery('#input_3_4_3').attr('placeholder', 'First Name');
  jQuery('#input_3_4_6').attr('placeholder', 'Last Name');

  // Selectbox on forms
  jQuery('.message select#input_1_1').addClass('dark-grey selectbox');
  jQuery('#gform_fields_3 .gfield_select').removeClass('medium').addClass('selectbox');

  // Custom Selectbox
  jQuery(".selectbox").selectBox();

  // Slideshow
  jQuery('.slideshow-wrap').cycle({
    fx: 'fade',
    slides: "> div",
    timeout: 7000,
    pager: '#slideshow-pager'
  });

  // show/hide modal
  jQuery('.series').click(function(e){
    jQuery('#modal').fadeIn();
  });
  jQuery('#close').click(function(){
    jQuery('#modal').hide();
  });

  // Slider-news
  jQuery('#slider-news').cycle({
    fx: 'turnDOwn',
    slides: "> div",
    timeout: 10000
  });

  // Carousel
  jQuery('.carousel').cycle({
    fx: 'carousel',
    visible: 2,
    slides: "img",
    timeout: 4000,
    next: '.nav-slide .next',
    prev: '.nav-slide .prev'
  });

  // Price hover
  jQuery('.prices').hover(function(){
    jQuery('.prices-wrap').fadeIn();
  }, function(){
    jQuery('.prices-wrap').fadeOut();
  });

  jQuery('ul.sf-menu').superfish({
      autoArrows:  false,
      dropShadows: false,
      delay: 200,
  });
  
  
  jQuery('.mvn-shop-single-subscription  li.submenu-subscription').addClass('current_page_item');
  jQuery('.mvn-shop-single-product  li.submenu-concerts').addClass('current_page_item');
  jQuery('.single-composer  li.submenu-composer').addClass('current_page_item');
  jQuery('.single-composer  li.menu-learn').addClass('current-menu-item');
  
   
  jQuery('.single-staff .subpage ul.container li.page-item-3698').addClass('current_page_ancestor');
  jQuery('.single-staff .subpage ul.children li.page-item-3700').addClass('current_page_item');
  
  jQuery('.single-scholar li.submenu-events').addClass('current_page_ancestor');
  jQuery('.single-scholar li.subsubmenu-scholars').addClass('current_page_item');
  
  
  // 3rd level menu page manipulation
  jQuery('.subpage ul.container li.current_page_item ul.children, .subpage ul.container li.current_page_ancestor ul.children').appendTo('.sub-subpage');
  //sub menu from admin menues
  jQuery('.subpage ul.container li.current_page_item ul.sub-menu, .subpage ul.container li.current_page_ancestor ul.sub-menu ').appendTo('.sub-subpage');

  // Only show the 3rd level menu if they exist
  if ( jQuery('.sub-subpage ul').length > 0 ) {
     jQuery('.sub-subpage').show();
   } else {
     jQuery('.sub-subpage').hide();
   }


  // Newsletter
  jQuery('.newsletter-wrap .gform_wrapper').hide();
  jQuery('.newsletter-wrap .btn-signup').click(function(e){
    jQuery('.wrap').hide();
    jQuery('.newsletter-wrap .gform_wrapper').show();
    e.preventDefault();
  });

  // Artists & Composers
  jQuery('#artists-more a').click(function(e){
    e.preventDefault();
  jQuery(this).hide();
  jQuery('#artists-more div').show();
  jQuery.ajax({
    type: 'GET',
    url: jQuery(this).attr('href') + '#artists-list',
    dataType: 'html',
    success: function(output){
      result = jQuery(output).find('.list-inline');
    nextlink = jQuery(output).find('#artists-more a').attr('href');
    result.fadeIn().appendTo('#artists-list');
    jQuery('#artists-more div').hide();
    jQuery('#artists-more a').show();
    if ( nextlink != undefined )
      jQuery('#artists-more a').attr('href',nextlink);
    else
      jQuery('#artists-more').remove();
    }
    });
  });

  // Tabs Venue
  jQuery('.single-venue .tabs li a').click(function(e){
    jQuery(this).parents('ul').find('li').removeClass('active');
    jQuery(this).parent().addClass('active');
    jQuery('.single-venue .tab-content').hide();
    jQuery('.single-venue .tab-content'+jQuery(this).attr('href')).fadeIn();
    e.preventDefault();
  });
  
  /*// prettyPhoto lightbox
  jQuery("a[rel^='prettyPhoto']").prettyPhoto({
    deeplinking: false,
    social_tools: false
  });*/
  
  // Full background
  var imgLocation = '/wp-content/themes/pcms/';
  jQuery(".page-venues section.landing-page").css("background-image", "url("+imgLocation+"images/bg-venue-"+GenerateNumber(2)+".jpg)");

  //1 to max
  function GenerateNumber(max) {
    return Math.floor(Math.random()*max) + 1;
  }
  
});